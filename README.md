# Class Room Planner an MVVM UWP App using Template10#

![Classroomplanner.jpg](https://bitbucket.org/repo/zaejze/images/3073064670-Classroomplanner.jpg)




This app is useful for any teacher who needs a seating plant.
The user will first enter in the children, tables, and chairs the class has. The user can then select children who they don't want to sit together and finally generate a seating plan based on certain criteria.

### How do I get set up? ###

To set up this app in its current state you will need the latest version of visual studio and Windows 10
Simply clone or download the repository and run .sln file.

### Current State ###
The app currently works for the most part. I still need to do some testing and a few things need fixing and tidying up. 

### Created by ###

* Thomas Kitch 
* thomaswkitch@gmail.com
tomwk@twitter.com





![Classroom2.jpg](https://bitbucket.org/repo/zaejze/images/2896116213-Classroom2.jpg)